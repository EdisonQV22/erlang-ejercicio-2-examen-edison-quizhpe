-module(servidor).
-import(math,[sqrt/1]).
-import(string,[concat/2]).
-export([start/0, cliente/3]).
start() ->
    inets:start(httpd, [
        {modules, [
            mod_alias,
            mod_auth,
            mod_esi,
            mod_actions,
            mod_cgi,
            mod_dir,
            mod_get,
            mod_head,
            mod_log,
            mod_disk_log
        ]},
        {port, 8091},
        {server_name, "prueba"},
        {server_root, "C:/Users/Edison/Desktop/clasesErlang/examen"},
        {document_root, "C:/Users/Edison/Desktop/clasesErlang/examen/htdocs"},
        {erl_script_alias, {"/serverprueba", [servidor]}},
        {error_log, "error.log"},
        {security_log, "security.log"},
        {transfer_log, "transfer.log"},
        {mime_types, [
            {"html", "text/html"},
            {"css", "text/css"},
            {"js", "application/x-javascript"},
            {"json", "application/json"}
        ]}
    ]).

cliente(SessionID, _Env, _Input) ->
    Data= _Input,
    io:format("Data: ~p~n", [_Input]),    
    io:format("Data1: ~p~n", [_Env]),    
    %%%%%%%SOLUCIOOOOOOOON%%%%%%%%%
    String1=[ element(1, string:to_integer(Substr)) || Substr <- string:tokens(Data, "/ ")],
    [A|String2]= String1,
    [B|String3]= String2,
    [C|Y]= String3,
    [X|Z]=Y,
    TermR= ((B*B)-(4*A*C)),
    mod_esi:deliver(SessionID,
    if
        TermR < 0 ->
            StrD= concat(integer_to_list(TermR)," "),
            io:format("Discriminante: ~p~n", [StrD]),
            ["Content-Type: text/html\r\n\r\n","<html><body>La ecuación no tiene solución, ya que el valor del discriminante es negativo: </body></html>",[StrD]];
        TermR >= 0 ->
            Raiz= sqrt(TermR),
            X1= ((-1*B) + Raiz)/(2*A),
            X2= ((-1*B) - Raiz)/(2*A),
            D1= (X1*X),
            D2= (X2*X),
            StrA= concat(integer_to_list(A),", <b>b=</b> "),
            StrB= concat(integer_to_list(B),", <b>c=</b> "),
            StrC= concat(integer_to_list(C),", <b>x=</b> "),
            StrX= concat(integer_to_list(X),"<br><b>Cálculo de la formula general:<br>X1=</b> "),
            StrX1= concat(float_to_list(X1,[{decimals,3}]),"<br> <b>X2=</b> "),
            StrX2= concat(float_to_list(X2,[{decimals,3}]),"<br> <b>Cálculo de la distancia, usando X1 y X2:<br>Con X1, d=</b> "),
            StrD1= concat(float_to_list(D1,[{decimals,3}]),"<br> <b>Con X2, d=</b> "),
            StrD2= concat(float_to_list(D2,[{decimals,3}]),"<br><br><font size='4'><b>Autor:</b> Edison Quizhpe"),
            MostrandoA= concat("<font face='nunito,arial,verdana'><font size='6'><center><b>*********SOLUCIÓN*********<br><font size='5'> <br>Valores ingresados: <br>a=</b> ",StrA),
            MostrandoB= concat(MostrandoA,StrB),
            MostrandoC= concat(MostrandoB,StrC),
            MostrandoX= concat(MostrandoC,StrX),
            MostrandoX1= concat(MostrandoX,StrX1),
            MostrandoX2= concat(MostrandoX1,StrX2),
            MostrandoD1= concat(MostrandoX2,StrD1),
            MostrandoD2= concat(MostrandoD1,StrD2),
            Res=io:format("*********SOLUCION*********~nValores ingresados: ~na= ~p~nb= ~p~nc=~p~nx=~p~nCalculo de la formula general:~nX1= ~p~nX2= ~p~nCalculo de la distancia, usando X1 y X2:~nCon X1, d= ~p~nCon X2, d= ~p~n", [A,B,C,X,X1,X2,D1,D2]),
            Res,
            ["Content-Type: text/html\r\n\r\n","<html><body>
            <br> </body></html>",[MostrandoD2]];

        true ->
            io:format(" ")
    end
).
    